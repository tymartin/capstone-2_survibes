<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'firstname' => 'Admin',
            'middlename' => 'Admin',
            'lastname' => 'Admin',
            'contact' => '09933558987', 
            'gender' => 'Male', 
            'email' => 'admin@admin.com',
            'password' => Hash::make('admin1234'),
            'role_id' => 1
        ]); 

        DB::table('users')->insert([
            'firstname' => 'Ty',
            'middlename' => 'Ty',
            'lastname' => 'Ty',
            'contact' => '09933558987',
            'gender' => 'Male', 
            'email' => 'ty@email.com',
            'password' => Hash::make('12345678'),
            'role_id' => 2
        ]); 

    }
}
