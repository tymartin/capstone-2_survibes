<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
        	'name' => 'Pending'
        ]); 
         DB::table('statuses')->insert([
        	'name' => 'Complete'
        ]); 
          DB::table('statuses')->insert([
        	'name' => 'Cancelled'
        ]); 
        DB::table('statuses')->insert([
        	'name' => 'Denied'
        ]); 
    }
}
