<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('serial')->unique();
            $table->string('stat'); 
            $table->string('name'); 
            $table->float('price')->nullable()->default(0); 

            $table->unsignedBigInteger('item_id')->nullable(); 
            $table->foreign('item_id')
                    ->references('id')
                    ->on('items')
                    ->onDelete('set null')
                    ->onUpdate('set null'); 
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
