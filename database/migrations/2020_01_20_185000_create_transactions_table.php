<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_code')->unique();
            $table->float('total')->nullable();
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->unsignedBigInteger('number')->nullable();          

           $table->unsignedBigInteger('user_id')->nullable();
           $table->foreign('user_id')
               ->references('id')->on('users')
               ->onDelete('set null')
               ->onUpdate('set null');
            
            $table->unsignedBigInteger('status_id')->nullable()->default(1);
            $table->foreign('status_id')
                ->references('id')->on('statuses')
                ->onDelete('set null')
                ->onUpdate('set null');
    
            $table->unsignedBigInteger('stock_id')->nullable();
            $table->foreign('stock_id')
                ->references('id')
                ->on('stocks')
                ->onDelete('set null')
                ->onUpdate('set null');
            
            $table->string('serial')->default(0);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
