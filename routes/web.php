<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::delete('/carts/empty','CartController@empty')->name('carts.empty');
Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');

Route::resource('categories', 'CategoryController'); 
Route::resource('items', 'ItemController'); 
Route::resource('statuses', 'StatusController'); 
Route::resource('stocks', 'StockController'); 
Route::resource('carts', 'CartController'); 
Route::resource('transactions', 'TransactionController'); 

