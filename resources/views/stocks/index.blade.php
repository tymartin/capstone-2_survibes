@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card"> 
                     <!-- Start Card Header -->
                  <div class="card-header">{{ __('Survibes Stock List') }}</div> 

                    <!-- < Start Card Body  -->
                    <div class="card-body">

                    <!-- Filter bar  -->
                    <form action="" method="get">
                            <div class="row">
                                <!-- Filter Field   -->
                                    <div class="col"> 
                                        <select name="item" id="item" class="form-control">
                                            <option value="">All </option> 
                                            @foreach($items as $item)
                                                <option value="{{ $item->id}}">{{$item->name}}
                                                </option> 
                                            @endforeach
                                        </select>     
                                    </div> 
                    <!-- Filter Button   -->
                                <div class="col"> 
                                    <button class="btn btn-outline-primary">Stock Search</button> 
                                </div> 
                    </form> 
            
                    </div> 
                
                    <!-- Validation for Errors  -->
                    @if ($errors->all())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif  

                    <!-- Validation for Changes  -->
                    @if(Session::has('status'))
			        <div class="alert alert-success col-12">
			        	{{Session::get('status')}}
			        </div>
		            @endif
                    <hr> 
                    <!-- List of Stocks  -->
                    <div class="row"> 
                    @foreach($stocks as $stock)
                    <!-- Start of Card  -->
                        <div class="col-12 col-md-3 col-lg-3">
                            <div class="card">
                            <img src="/public/{{$item->image}}" alt="image unavailable" class="card-img-top">
                                <div class="card-body"> 
                                    <p class="card-text"><strong>{{ $stock->item->name}}</strong></p>

                                    <label for="serial"><small>Serial No.</small></label>
                                    <p class="card-text"><strong>{{ $stock->serial}}</strong></p>

                                    <label for="serial"><small>Status:</small></label>
                                    <p class="card-text"><strong>{{ $stock->stat}}</strong></p>
                                </div> 

                                <div class="card-footer">
                                <!-- Card Footer  -->
                                <!-- Request Button  -->
                                    <form action="{{ route('carts.update', ['cart'=>$stock->id])}}" method="post">
                                        @method('PUT')
                                        @csrf
                                        <button class="btn btn-outline-primary w-100 mb-2" {{($stock->stat == 'Not Available' ? 'disabled' : '')}}>Request</button>
                                     </form>

                                 <!-- Update Status  -->
                                    @can('isAdmin')
                                    <form action="{{ route('stocks.update', ['stock'=> $stock->id]) }}" method="post">
                                        @method('PUT')
                                        @csrf 
                                        <select name="stat" id="stat" class="form-control">
                                            <option disabled selected>Select Status</option> 
                                            <option value="Available">Available </option> 
                                            <option value="Not Available">Not Available </option> 
                                        </select>
                                        <button class="btn btn-outline-primary w-100 mb-2">Update Status</button>
                                    </form> 
                                <!-- Delete Stock  -->
                                    <form action="{{ route('stocks.destroy', ['stock'=> $stock->id])}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger w-100 mb-2">Delete Stock</button>
                                    </form> 

                                    @endcan
                                </div> 
                            </div>  
                        </div> 
                         <!-- End of Card  -->
                        @endforeach
                    </div>
                </div> 
                   
            </div>
        </div>
    </div> 
</div> 
@endsection