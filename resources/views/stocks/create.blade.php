@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-7">
            <div class="card"> 
            @if ($errors->all())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <h6 class="card-header">Create New Stock for Item</h6>
                <div class="card-body">

                <form action="{{ route('stocks.store')}}" method="post">
				@csrf

                    <!-- Item Name Select -->
                    <select name="item_id" id="item_id" class="custom-select" mb-2>
                   
					@foreach($items as $item)
						<option value="{{ $item->id }}" {{ old('item_id') == $item->id ? "selected":""}}>
						{{ $item->name }}
						</option>
					@endforeach
					</select>     
                    <hr> 
                     <!-- Name Field -->
                    <label for="name" hidden>Item Name</label>
                    <div> 
                    <input type="text" name="name" autocomplete="off" class="form-control" mb-2 placeholder="Enter Item Name" hidden>  
                    </div>

                    <!-- Price Field  -->
                    <label for="price">Item Price</label>
                    <div>
                    <input type="text" name="price" autocomplete="off" class="form-control" mb-2 placeholder="Enter Rental Fee" hidden>  
                    </div>       

                    <!-- Item Status  -->
                    <label for="status">Item Status</label>
                    <select name="stat" id="stat" class="form-control"> 
                        <option value="Available">Available </option> 
                        <option value="Not Available">Not Available </option> 
                    </select>

                    <div> 
                        <hr>    
                        <button type="submit" class="btn btn-warning mb-2">Generate Item Stock</button>
                    </div> 
								
                </form> 
                <div>
            </div>
        </div>
    </div>
</div> 
@endsection