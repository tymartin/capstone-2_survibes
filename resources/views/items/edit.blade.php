@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-12 col-md-7 ">
            <div class="card"> 
                @if ($errors->all())
                    <div class="alert alert-danger">
                            <ul>
                                 @foreach ($errors->all() as $error)
                                     <li>{{ $error}}</li>
                                 @endforeach
                            </ul>
                    </div>
                @endif
                        <form action="{{ route('items.update', ['item'=>$item->id])}}" method="post" enctype="multipart/form-data">   
                            @csrf 
                            @method('PATCH')
                                <!-- Start Card Header -->
                                <div class="card-header">{{ __('Create New Item') }}</div>

                                <!-- Start Card Body -->
                                <div class="card-body">

                                        <!-- Name Field -->
                                        <div> 
                                        
                                            <input type="text" name="name" autocomplete="off" class="form-control" mb-2 placeholder="Enter Item Name" 
                                            value="{{ $item->name}}">  
                                            
                                        
                                        </div>
                                        <hr>  
                                        <!-- Price Field  -->
                                        <div>
                                            
                                            <input type="text" name="price" autocomplete="off" class="form-control" mb-2 placeholder="Enter Rental Fee"  
                                            value="{{ $item->price}}">  
                                        
                                        </div> 
                                        <hr>  

                                        <!-- Category Field  -->
                                        <div>
                                            
                                            <select name="category_id" id="category_id" class="custom-select" mb-2>
                                                <option value="{{$item->category_id}}">Choose Item Category</option>
                                                @foreach($categories as $categories)
                                                <option value="{{ $categories->id }}"{{ old('category_id') == $categories->id ? "selected":""}}>
                                                {{ $categories->name}}
                                                </option>
                                                @endforeach
                                            </select>
                                                
                                        
                                        </div>

                                        <hr>    
                                        <!-- Image Field  -->

                                        <input type="file" name="image" id="image" class="form-control-file mb-2" value="{{$item->image}}">
                                    
                                    
                                        <hr> 
                                            <div class="row">   
                                                    <!-- Color Field  -->
                                                        <div class="col-4">
                                                        
                                                                <label for="color" class="row-md-1 col-form-label text-md-right ml-1">{{ ('Color') }}</label> 
                                                    

                                                                <div class="col-12 w-100">
                                                                    <select name="color" id="color" class="form-control">
                                                                        <option value="{{$item->color}}">{{$item->color}}</option> 
                                                                        <option value="/">Not Available </option> 
                                                                        <option value="Blue">Blue </option> 
                                                                        <option value="Light Blue">Light Blue </option> 
                                                                        <option value="Dark Blue">Dark Blue </option> 
                                                                        <option value="Green">Green </option> 
                                                                        <option value="Magenta">Magenta </option> 
                                                                        <option value="Magenta">Red </option> 
                                                                        <option value="Yellow">Yellow </option> 
                                                                        <option value="Magenta">Violet </option> 
                                                                    </select> 
                                                                </div> 
                                                        </div>         
                                                            
                                                    
                                                    <!-- Brand Field -->
                                                        
                                                    <div class="col-4">
                                                                <label for="brand" class="row-md-1 col-form-label text-md-right ml-1">{{ ('Brand') }}</label>    

                                                                <div class="col-12 w-100">
                                                                    <select name="brand" id="brand" class="form-control"> 
                                                                    <option value="{{$item->brand}}">{{$item->brand}}</option> 
                                                                    <option value="Generic">Generic </option> 
                                                                        <option value="Amazon">Amazon </option> 
                                                                        <option value="Speedo">Speedo </option> 
                                                                        <option value="Copozz">Copozz </option> 
                                                                        <option value="Izod">Izod </option> 
                                                                        <option value="Columbia">Columbia </option> 
                                                                        <option value="Smaco">Smaco </option> 
                                                                        <option value="Atomic Aquatics">Atomic Aquatics </option> 
                                                                    </select>
                                                                </div> 
                                                                
                                                            
                                                        </div>   
                                                    
                                                    <!-- Size Field -->
                                                    

                                                    <div class="col-4">
                                                                <label for="size" class="row-md-1 col-form-label text-md-right ml-1">{{ ('Size') }}</label>  
                                                            
                                                                <div class="col-12 w-100">
                                                                    <select name="size" id="size" class="form-control"> 
                                                                    <option value="{{$item->size}}">{{$item->size}}</option> 
                                                                    <option value="All">All </option> 
                                                                    <option value="Adult">Adult </option> 
                                                                    <option value="Kids">Kids </option> 
                                                                    <option value="XL">XL </option> 
                                                                    </select> 
                                                                </div> 
                                                                
                                                                    
                                                    </div>
                                                    
                                                
                                            
                                            </div> 
                                        
                                                <br>    
                                            <button type="submit" class="btn btn-primary">Save Updates</button>      
                                        
                                </div>
                      </form> 
                </div>
                </div> 
                    <div class="col-12 col-md-5">
                        <img src="/storage/{{$item->image}}" alt="image unavailable" class="card-img-top">
                            <div class="card-body">
                                <h3>{{$item->name}}</h3>
                                <p class="card-text">&#8369; {{number_format($item->price,2)}}</p>
                                <p class="card-text">{{ $item->category->name}}</p>
                                <p class="card-text">{{ $item->brand}}</p>
                                <p class="card-text">Color : {{ $item->color}}</p>
                                <p class="card-text">Size  : {{ $item->size}}</p>
                            </div>
                    </div>
            </div>
        </div> 

</div> 


@endsection 