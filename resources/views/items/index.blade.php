@extends('layouts.app')

@section('content') 

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card"> 
                     <!-- Start Card Header -->
                  <div class="card-header">{{ __('Survibes Item List') }}</div> 

                        <!-- < Start Card Body  -->
                        <div class="card-body">

                            <!-- Filter bar  -->
                            <form action="" method="get">
                                    <div class="row">
                                        <!-- Filter Field   -->
                                        <div class="col"> 
                                            <select name="category" id="category" class="form-control">
                                                <option value="">All</option> 
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id}}">{{$category->name}}
                                                    </option> 
                                                @endforeach
                                            </select>     
                                        </div> 
                                        <!-- Filter Button   -->
                                        <div class="col"> 
                                            <button class="btn btn-outline-primary">Category Search</button> 
                                        </div>
                                    </div> 
                            </form> 
                            <hr> 
                            <!-- Validation for Errors  -->
                            @if ($errors->all())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                             @endif
                            <!-- Validation for Changes  -->
                             @if(Session::has('status'))
		                    	<div class="alert alert-success col-12">
			                    	{{Session::get('status')}}
			                    </div>
	                    	@endif
                             <!-- Card for Item  -->
                            <div class="row">
                             @foreach($items as $item)
                                <!-- Start of Card  -->
                                <div class="col-12 col-md-4 col-lg-3">
                                    <div class="card">
                                      <img src="/public/{{$item->image}}" alt="image unavailable" class="card-img-top">
                                        <div class="card-body">
                                            <h5 class="card-title">{{$item->name}}</h5>
                                            <p class="card-text">&#8369; {{ number_format($item->price,2)}}</p>
                                            <p class="card-text">{{ $item->category->name}}</p>
                                            <p class="card-text">{{ $item->brand}}</p>
                                            <p class="card-text">Color : {{ $item->color}}</p>
                                            <p class="card-text">Size  : {{ $item->size}}</p>
                                            <p class="card-text">Stocks : {{ $item->stock}}</p>
                                        </div>
                                        <div class="card-footer">
                                              
                                            <!-- View Item  -->
                                            <a href="{{ route('items.show',['item' => $item->id])}}" class="btn btn-outline-success w-100 mb-2">View Item</a>
                                            <!-- Edit Item  -->
                                            @can('isAdmin')
                                            <a href="{{ route('items.edit',['item' => $item->id])}}" class="btn btn-outline-warning w-100 mb-2">Edit Item</a>

                                            <!-- Delete Item  -->
                                            <form action="{{ route('items.destroy',['item' => $item->id])}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                                <button type="submit" class="btn btn-outline-danger w-100 mb-2">Delete</button>
                                            </form> 
                                            @endcan


                                        </div>
                                    </div>
                                </div>
                                <!-- End of Card  -->
                             @endforeach
                            </div> 





                            
                            

                         </div> 

            </div> 
        </div> 
    </div> 
</div> 




@endsection  