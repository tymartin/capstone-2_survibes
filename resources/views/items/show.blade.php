@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card"> 
            @if ($errors->all())
                    <div class="alert alert-danger">
                            <ul>
                                 @foreach ($errors->all() as $error)
                                     <li>{{ $error}}</li>
                                 @endforeach
                            </ul>
                    </div>
                @endif  
              <img src="/public/{{$item->image}}" alt="image unavailable" class="card-img-top">
                     <div class="card-body"> 
                     <form action="{{ route('stocks.store')}}" method="post">
			           @csrf
                     <h3>{{$item->name}}</h3>
                                <p class="card-text">&#8369; {{number_format($item->price,2)}}</p>
                                <p class="card-text">{{ $item->category->name}}</p>
                                <p class="card-text">{{ $item->brand}}</p>
                                <p class="card-text">Color : {{ $item->color}}</p>
                                <p class="card-text">Size  : {{ $item->size}}</p>
                    </div> 

                    <div class="card-footer"> 

                    <a href="{{ route('stocks.index')}}" class="btn btn-outline-primary w-100 mb-2">See Available Stocks</a>
                        @can('isAdmin') 
                    
                      <!-- Item Id -->
                      <label for="item_id" hidden>Item ID</label>
                    <div> 
                    <input type="text" name="item_id" autocomplete="off" class="form-control" mb-2 value="{{ $item->id }}" hidden >  
                    </div>

                       <!-- Name Field -->
                    <label for="name" hidden>Item Name</label>
                    <div> 
                    <input type="text" name="name" autocomplete="off" class="form-control" mb-2 value="{{ $item->name }}" hidden >  
                    </div>
                    
                     <!-- Price Field  -->
                     <label for="price" hidden>Item Price</label>
                    <div>
                    <input type="text" name="price" autocomplete="off" class="form-control" mb-2 value="{{ $item->price }}" hidden>  
                    </div> 
                     
                    <!-- Item Status  -->
                    <label for="status" hidden>Item Status</label>
                    <select name="stat" id="stat" class="form-control" hidden> 
                        <option value="Available">Available </option> 
                        <option value="Not Available">Not Available </option> 
                    </select> 
                    <hr> 
                    <!-- Create New Stock Button   -->
                    <form action="{{ route('stocks.create')}}">
                            <button class="btn btn-warning w-100 mb-2 text-white">Create New Stock</button>
                    </form>
                        <!-- Edit Item  -->
                        <a href="{{ route('items.edit',['item' => $item->id])}}" class="btn btn-outline-warning w-100 mb-2">Edit Item</a>

                        <!-- Delete Item  -->
                        <form action="{{ route('items.destroy',['item' => $item->id])}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-outline-danger w-100 mb-2">Delete</button>
                        </form>
                                
                        @endcan 

                        

            </div>
        </div>
    </div> 
</div> 


@endsection 