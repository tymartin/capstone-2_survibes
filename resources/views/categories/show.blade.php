@extends('layouts.app')

@section('content')
 
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card"> 
                  <!-- Start Card Header -->
                  <div class="card-header">{{ __('Category Details') }}</div>
                
                   <!-- Start Card Body  -->
                      <div class="card-body">

                            <div>
                                    <strong> Category Name</strong>
                                    <h4>{{ $category->name }} </h4>    
                            </div>

                            <div>
                                    <strong>created at:</strong>
                                    <h6>{{ $category->created_at }} </h6>    
                            </div>
                            <div>
                                    <strong>updated at:</strong>
                                    <h6>{{ $category->updated_at }} </h6>    
                            </div>

                            <div> <hr> 

                                    <button> 
                                        <a href="/categories">Back</a>
                                    </button> 
                            </div> 

                            <div> 

                                    <button> 
                                        <a href="/categories/{{ $category->id }}/edit">Edit</a>
                                    </button> 
                            </div> 

                            <div> 
                                 <form action="/categories/{{ $category->id }}" method="post">  
                                  @method('DELETE') 
                                  @csrf  
                                        <button> 
                                            Delete
                                        </button> 
                                </form> 
                            </div> 

                      </div> 

            </div> 
        </div> 
    </div> 
</div> 




@endsection 