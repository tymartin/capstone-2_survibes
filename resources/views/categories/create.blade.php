@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card"> 
               <!-- Start Card Header -->
                <div class="card-header">{{ __('Create New Category') }}</div>
               <!-- Start Card Body -->
                <div class="card-body">
                    <form action="/categories" method="post">
                        @csrf
                        <div>
                            <label for="name">Category name:</label>
                            <input type="text" name="name" autocomplete="off" value="{{ old('name') ?? $category->name}}">    
                            @error('name') <p style="color:red;"> {{ $message }} </p> @enderror
                        </div>
                        <div> 
                        <button>Add New Category</button>
                        </div> 
                    </form> 
                </div> 
                <!-- End Card Body -->
            </div>
        </div> 
    </div> 
</div> 


@endsection 