@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                    <div class="card-header">Categories</div> 

                        <div class="card-body">

                           <button><a href="/categories/create">Add New Category</button></a>
                            @forelse ($categories as $category)
                                <p><strong><a href="/categories/{{ $category->id }}">{{ $category->name }} </a></strong></p>
                            @empty
                                <p>No Categories to Show!</p>
                            @endforelse 
                        </div> 
            </div>
        </div> 
    </div> 
</div>
@endsection 