@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card"> 

                  <!-- Start Card Header -->
                  <div class="card-header">{{ __('Category Details') }}</div>
                
                   <!-- Start Card Body  -->
                      <div class="card-body">
                            <form action="/categories/{{ $category->id }}" method="post">
                                @method('PATCH')
                                    
                                    <div> 
                                        <label for="name">Category name:</label>
                                        <input type="text" name="name" autocomplete="off" value="{{ old('name') ?? $category->name}}">    
                                        @error('name') <p style="color:red;"> {{ $message }} </p> @enderror
                                    </div> 
                                @csrf 
                                @can('isAdmin')
                                    <div> 
                                        <button>Update Category</button>
                                    </div> 
                                @endcan
                            </form> 
                      </div> 
            </div> 
        </div> 
    </div> 
</div> 





@endsection 