@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
			<h5>Item Request form</h5>
		</div>

         <!-- Validation for Errors  -->
        @if ($errors->any())
	    <div class="alert alert-danger col-12">
	    	<ul>	
			    @foreach($errors->all() as $error)
			    <li>{{ $error }}</li>
			    @endforeach
		    </ul>
	    </div>
        @endif

         <!-- Validation for Changes  -->
         @if(Session::has('status'))
			<div class="alert alert-success col-12">
			    {{Session::get('status')}}
			</div>
		  @endif

        @if(Session::has('cart'))
        <div class="col-md-12">
            <div class="table table-responsive">
                <table class="table table-striped table-hover text-center">
                    <thead>
                        <th scope="col">Item Name</th>
                        <th scope="col">Serial No.</th>
                        <th scope="col">Rental Price</th>
                        <th scope="col">Request Actions</th>
                    </thead>
                    <tbody>
                    @foreach($stocks as $stock)
                       <tr>
                            <td>{{ $stock->item->name}}</td>
                            <td>{{ $stock->serial}}</td>
                            <td>  &#8369;{{number_format($stock->item->price,2)}}</td>
                            <td>
                                <!-- Item Delete  -->
                                <form action="{{ route('carts.destroy', ['cart'=> $stock->id])}}" method="post">
									@csrf
									@method('DELETE')
                                    <button class="btn btn-danger w-100 mb-2">Cancel</button>
                                </form> 
                            </td>
 
                        </tr> 
                        @endforeach
                        <tr>
                            <td colspan="3" class="text-right">Total</td>
							<td>&#8369; <span id="total">{{ number_format($total,2) }}</span></td>
                        </tr> 
                    </tbody>
                </table>     
            </div>
            <!-- Request Form Delete  -->
            <form action="{{ route('carts.empty')}}" method="post">	
				@csrf
				@method('DELETE')
	            <button type="submit" class="btn btn-dark">Delete Request Form</button>
            </form>
            @can('isLogged')
            <form action="{{ route('transactions.store')}}" method="post">																					
			@csrf
			<button class="btn btn-primary mt-2">Submit Request Form</button>
			</form>
            @endcan
        </div> 
        @else
            <div class="col-12">
               	<div class="alert alert-info">
               	Cart Empty
            </div>
        @endif 
    </div> 
</div>
@endsection 