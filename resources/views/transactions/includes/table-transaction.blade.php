<!-- Start of transaction table -->
<table class="table table-striped">
	<tbody>
          <table class="table table-striped">
	<tbody>
        <tr>
			<!-- Transaction Code -->
			<td> Transaction Code: </td>
			<td><strong>{{ $transaction->transaction_code }}</strong></td>
		</tr>

        <tr>
			<!-- Customer Name -->
			<td>Customer Name:</td>
			<td>{{ $transaction->user->firstname }} {{ $transaction->user->lastname }}</td>
		</tr>

        <tr>
			<!-- Customer Email -->
			<td>Customer Email:</td>
			<td>{{ $transaction->user->email }}</td>
		</tr>

        <tr>
			<!-- Contact Details -->
			<td> Contact Details: </td>
			<td>{{ $transaction->user->contact }}</td>
		</tr>

        <tr>
			<!-- Customer Sex -->
			<td>Sex:</td>
			<td>{{ $transaction->user->gender }}</td>
		</tr>

        <tr>
			<!-- Rental Date -->
			<td> Rental Date: </td>
			<td>
			{{ $transaction->created_at->format('F d, Y h:i:s') }}
			</td>
		</tr>

		<tr> 
			<!-- Request Status -->
			<td>Request Status:</td>
			<td>
					<p class="text-white col-3 bg-warning"><strong>{{ $transaction->status->name }}</strong></p>
					@can('isAdmin')
					<form action="{{ route('transactions.update', ['transaction'=>$transaction->id])}}" method="post">
						@csrf
						@method('PUT')

						<select name="status" id="status" class="form-control">
							@foreach($statuses as $status)
								<option value="{{$status->id}}" {{$transaction->status_id == $status->id ? 'selected':''}}>
								{{$status->name}}
								</option>
							@endforeach
						</select>
						<button class="btn btn-warning">Edit Status</button>
					</form>
					@endcan
			</td>
		</tr> 

    </tbody> 
</table> 
<!-- end of transaction table -->




