@extends('layouts.app')

@section('content') 


<div class="container">
	<div class="row">
		<div class="col-12">
		<h4>Store Transactions</h4>
		</div>
	</div>

    <div class="col-12">
		<div class="accordion" id="accordionExample">
        <!-- start of accordion -->
        @foreach ($transactions as $transaction)
    	<div class="card">

		<div class="card-header" id="headingOne">
		<h5 class="mb-0">
		<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#transaction-{{ $transaction->id }}" aria-expanded="true" aria-controls="collapseOne">
		<span class="badge badge-success float-right">Transaction #{{ $transaction->id }} : {{ $transaction->transaction_code}} </span>
		<span class="badge badge-warning float-right">{{ $transaction->created_at->format('F d,Y') }} </span>  
		<span class="badge badge-info float-right"> {{$transaction->status->name}}</span>

        </button>
        </h5> 
        </div> 

		<div id="transaction-{{ $transaction->id }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
		<div class="card-body">
		<!-- start of table -->
		<div class="table-responsive">
		<!-- start of transaction table -->
		@include('transactions.includes.table-transaction')
		<!-- end of table product_transaction -->
		</div> 
		<!-- end of table -->
		</div> 
		</div>
		</div>
		@endforeach
		<!-- end of accordion -->
        </div> 
	</div>
</div>





@endsection









