@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="card text-white bg-warning">
				<h4 class="card-header">My Request Status</h4>
					<div class="card-body bg-success">
						<p class="card-text">If and Item is not returned in 24 hours it will be considered sold, thus forfeiting the deposit fee.</p>
					</div>
				</div>
			
		</div>
			<div class="col-12">
			<!-- start of table -->
			<div class="table-responsive">
			<!-- start of transaction table -->


            @include('transactions.includes.table-transaction')

            </div> 
			<!-- End of transaction table -->
            </div> 
		
			<table class="table table-striped table-hover">
			<!-- Start of Stock Order Request -->
					<thead>
						<th scope="row">Item Name</th>
						<th scope="row">Serial No.</th>
						<th scope="row">Status</th>
						<th scope="row">Price</th>
					</thead>
					<tbody>
					@foreach($transaction->stocks as $transaction_stock)
						<tr>
							<td>
							{{$transaction_stock->name}}
							</td>
							<td> 	
							{{$transaction_stock->serial}}	
							</td>
							<td>
							{{$transaction_stock->stat}}		 
							</td>
							<td>
								&#8369; {{ number_format($transaction_stock->price, 2) }}	
							</td>
						</tr>	
					@endforeach
					</tbody>
					<tfoot>
						<tr>
							<td colspan="3" class="text-right"><strong>Total</strong></td>
							<td>&#8369;  {{ number_format($transaction->total,2) }} </td>
						</tr>
					</tfoot>
				<!-- End of Stock Order Request -->
				</table>
   </div> 
@endsection
