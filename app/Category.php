<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function items($value='')
    {
        return $this->hasMany('App\Item'); 
    }
    protected $fillable = [
        'name'];
}
