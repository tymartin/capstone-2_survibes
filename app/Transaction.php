<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function status()
    {
    	return $this->belongsTo('App\Status');
    }
    public function user()
    {
    	return $this->belongsTo('App\User');
    }
    public function stocks()
    {
    	return $this->belongsToMany(
    		'App\Stock','stock_transaction')
            ->withPivot('price');
    } 
}
