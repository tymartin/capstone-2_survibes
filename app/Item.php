<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
    public function stock()
    {
        return $this->hasMany('App\Stock');
    }
    public function transaction()
    {
    	return $this->belongsToMany(
    		'App\Item','item_transaction')
    		->withPivot('subtotal', 'price');
    }

    protected $fillable = [
        'name', 'price', 'image', 'brand', 'color', 'size', 'category_id'];
}
