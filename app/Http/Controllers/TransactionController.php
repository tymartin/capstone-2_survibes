<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Stock;
use App\Status; 
use Illuminate\Http\Request;
use Str;
use Auth;
use Session;


class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Transaction $transaction)
    {   
        $this->authorize('view', $transaction); 
        $statuses = Status::all();
        $transactions = Transaction::all();
        return view ('transactions.index')->with('transactions', $transactions)->with('statuses', $statuses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Stock $stock)
    {
    
       $transaction = new Transaction;
       $transaction->user_id = Auth::user()->id;
       $transaction->transaction_code = Auth::user()->id . Str::random(10);
    

       $transaction->save();

       $stock_ids = array_keys(Session::get('cart'));
       $stocks = Stock::find($stock_ids);
       $total = 0; 
       foreach ($stocks as $stock)
       {
        $stock->subtotal = $stock->price * 1;
        $total += $stock->subtotal;


       $transaction->stocks()
       ->attach(
           $stock->id, 
           [ 
            "subtotal" => $stock->subtotal,
            "price" => $stock->price
           ]
       );
    }
       $transaction->total = $total;
       $transaction->save();

       Session::forget("cart");

       return redirect( route('transactions.show',['transaction'=>$transaction->id]))->with('stocks', $stocks); 

       

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
       $statuses = Status::all(); 
       return view('transactions.show')
              ->with('transaction', $transaction)
              ->with('statuses', $statuses); 

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $transaction->status_id = $request->status;
        $transaction->save();
        return redirect( route('transactions.index')); 
   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
