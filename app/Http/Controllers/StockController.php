<?php

namespace App\Http\Controllers;

use App\Item;  
use App\Stock;
use Str; 
use Illuminate\Http\Request;



class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request, Stock $stock)
    {
        
        $filter = $request->query('item'); 
            if($filter){
               $stocks = Stock::all()->whereIn('item_id', $filter);
            } else {
                $stocks = Stock::all(); 
            }
        return view('stocks.index')
        ->with('stocks', $stocks)
        ->with('items', Item::all()); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Stock $stock)
    {
        $this->authorize('create',$stock);
        $items = Item::all(); 
        return view('stocks.create')->with('items', $items); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Stock $stock)
    {
        $this->authorize('view',$stock);
        $request->validate([
            'item_id' => 'required',
            'stat' => 'required',   
            'name' => 'required',
            'price' => 'required',  
        ]);  

        $stock = new Stock;
        $stock->item_id = $request->input('item_id');
        $stock->name = $request->input('name');
        $stock->price = $request->input('price');
        $stock->serial = Str::random(12); 
        $stock->stat = $request->input('stat');

        $stock->save(); 

        $item_stocks = Item::all(); 

       foreach ($item_stocks as $items) {
           $stocks = Stock::all()
            ->whereIn('item_id', $items->id)
            ->whereIn('stat', 'Available');
            $items->stock = count($stocks);
            $items->save();  
       }

        return redirect( route('stocks.index', ['stock' => $stock->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $items = Item::all(); 
        return view ('stocks.show')->with('items', $items)  ; 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function edit(Stock $stock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stock $stock)
    {
        $request->validate([
        
            'stat' => 'required',   
        ]);  
         
       $stock->stat = $request->input('stat'); 
      
    
       $stock->save();
       
       $item_stocks = Item::all(); 

       foreach ($item_stocks as $items) {
           $stocks = Stock::all()
            ->whereIn('item_id', $items->id)
            ->whereIn('stat', 'Available');
            $items->stock = count($stocks);
            $items->save();  
       }
       return redirect( route('stocks.index', ['stock' => $stock->id])); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stock $stock)
    {
        $stock->delete();

        $item_stocks = Item::all(); 

        foreach ($item_stocks as $items) {
            $stocks = Stock::all()
             ->whereIn('item_id', $items->id)
             ->whereIn('stat', 'Available');
             $items->stock = count($stocks);
             $items->save();  
        }
        return redirect( route('stocks.index'))->with('status', 'Stock Removed');

    }
}
