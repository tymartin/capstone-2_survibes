<?php

namespace App\Http\Controllers;

use App\Category; 
use App\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }
    public function index(Request $request, Item $item)
    {  
        // $this->authorize('view', $item); 
        $filter = $request->query('category'); 
            if($filter){
                $items = Item::all()->whereIn('category_id', $filter);
            } else {
                $items = Item::all(); 
            }
        return view('items.index')
            ->with('items', $items)
            ->with('categories', Category::all()); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Item $item)
    {
        $this->authorize('create',$item);
        $categories = Category::all(); 
        return view('items.create')
            ->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Item $item)
    {   
        
        $request->validate([
         'name' => 'required|string',
         'price' => 'numeric',
         'category_id' => 'required',
         'image' => 'required|image|max:5000',
         'color' => 'required',
         'brand' => 'required',
         'size' => 'required'
        ]);  
        
        $item = new Item;
        $item->name = $request->input('name');
        $item->price = $request->input('price'); 
        $item->stock = $request->input('stock'); 
        $item->category_id = $request->input('category_id');
        $item->color = $request->input('color');
        $item->brand = $request->input('brand');
        $item->size = $request->input('size');     
        $item->image = $request->image->store('public','public');
   
        $item->save(); 

        return redirect( route('items.index')); 

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        return view('items.show')->with('item', $item); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        $categories = Category::all();
        return view("items.edit", compact('item'))->with('categories', $categories); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $request->validate([
            'name' => 'required|string',
            'price' => 'numeric',
            'category_id' => 'required',
            'image' => 'required|image|max:5000',
            'color' => 'required',
            'brand' => 'required',
            'size' => 'required'
           ]);  
        
           $item->name = $request->input('name');
           $item->price = $request->input('price'); 
           $item->stock = $request->input('stock'); 
           $item->category_id = $request->input('category_id');
           $item->color = $request->input('color');
           $item->brand = $request->input('brand');
           $item->size = $request->input('size');  

           if($request->hasFile('image'))
           {
            $item->image = $request->image->store('items');
           }
           $item->save();
           
           $request->session()->flash('status', 'update successful'); 

          return redirect( route('items.edit',['item'=>$item->id]));   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $item->delete();
        return redirect(route('items.index'))->with('status', 'Item is Deleted'); 
    }
}
