<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Stock; 
use Illuminate\Http\Request;
use Session; 
use App\Item;


class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        if(Session::has('cart'))  
        { 
        $stock_ids = array_keys(Session::get('cart'));
        $stocks = Stock::find($stock_ids);

        $total = 0; 
        foreach($stocks as $stock)
        {
            $stock->name = Session::get("cart.$stock->id"); 
            $stock->price =  $stock->price; 
            $total += $stock->price;  
            
        }
       
        return view('carts.index')
            ->with('stocks', $stocks)->with('stocks', $stocks)->with('total', $total); 
        } else {
            return view('carts.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $request->session()->put("cart.$id"); 

        return redirect(route('carts.index'))->with('status', 'Item added to Cart'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $request->session()->forget("cart.$id");

        if (count($request->session()->get('cart')) == 0) 
        {
            $request->session()->forget("cart");
        }
 
        return redirect( route('carts.index'))->with('status', 'Request Removed');
    }
    public function empty()
    {
       Session::forget('cart');
       return redirect( route('carts.index'))->with('status','Cart Cleared');
   }
}
