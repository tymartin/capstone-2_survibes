<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Item;
use App\Category;
use App\Stock;
use App\Policies\ItemPolicy;
use App\Policies\CategoryPolicy;
use App\Policies\StockPolicy;
use App\Policies\TransactionPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Item::class => ItemPolicy::class,
        Category::class => CategoryPolicy::class,

        'App\Transaction' => 'App\Policies\TransactionPolicy'
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isAdmin', function($user){
            return $user->role_id ===1;
        });

        Gate::define('isLogged', function($user){
            return $user !== null;
        });
    }
}
