<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    public function item()
    {
        return $this->belongsTo('App\Item');
    }
    public function transaction()
    {
    	return $this->belongsToMany(
    		'App\Stock','stock_transaction')
    		->withPivot('subtotal', 'price');
    }

    protected $fillable = [
        'item', 'serial', 'stat'];
}
